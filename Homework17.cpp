#include <iostream>
#include <cmath>

class Vector
{
    int x = 0;
    int y = 0;
    int z = 0;

public:
    Vector()
        {}
    Vector(int x1, int  y1, int z1) : x(x1), y(y1), z(z1)
        {}

    double length()
    {
        return std::sqrt(x * x + y * y + z * z);
    }
};

int main()
{
    Vector v(3, 4, 5);

    std::cout << v.length() << '\n';
}

